# Shader-Project

A GLSL shader experiment.


Testing GLSL shaders. Specifically for Minecraft 1.12.2 +  cause of BlockID issues.

Can be used in other renderers but need to change some code (TBD).

Use Optifine or GLSL forge mod to render. (Mind your PC, it burns)

Currently only composites and sky usable in external renderers.
