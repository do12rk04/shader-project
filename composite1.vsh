//Test shader - based around multiple shaders

#version 460
#define vert

#include "/InternalLib/Syntax.glsl"

varying vec2 texcoord;

void main() {
	gl_Position = vec4(gl_Vertex.xy * 2.0 - 1.0, 0.0, 1.0);

    texcoord = gl_Vertex.xy;

}
