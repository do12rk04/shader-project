//Test shader - based around multiple shaders

#include "/UserLib/CameraSettings.glsl"
#include "/UserLib/ToningSettings.glsl"
#include "/UserLib/VolumetricSettings.glsl"
#include "/UserLib/ParallaxSettings.glsl"
#include "/UserLib/LightingSettings.glsl"
#include "/UserLib/SkySettings.glsl"
